package ru.am.cops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CopsApplication.class, args);
    }

}
